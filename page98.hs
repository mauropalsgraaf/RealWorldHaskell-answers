module Page98 (module Page98) where
import Data.Char

myAsInt :: String -> Int
myAsInt "" = 0
myAsInt "-" = 0 
myAsInt input
  | head input == '-' = 
    let positiveInt = foldl (\a b -> a * 10 + digitToInt b) 0 $ tail input in
      positiveInt - (2 * positiveInt)
  | otherwise = foldl (\a b -> a * 10 + digitToInt b) 0 input

myAsIntEither :: String -> Either String Int
myAsIntEither "" = Right 0
myAsIntEither "-" = Right 0
myAsIntEither input
  | not $ areAllDigits input = Left "not all digits"
  | head input == '-' = 
    let 
      positiveInt = foldl (\a b -> a * 10 + digitToInt b) 0 $ tail input
    in
      Right $ positiveInt - (2 * positiveInt)
  | otherwise = Right $ foldl (\a b -> a * 10 + digitToInt b) 0 input

areAllDigits :: String -> Bool
areAllDigits [] = True
areAllDigits xs = foldr ((&&) . isDigit) True xs

myConcat :: [[a]] -> [a]
myConcat = foldr (++) []

takeWhileRec :: (a -> Bool) -> [a] -> [a]
takeWhileRec _ [] = []
takeWhileRec f (x:xs)
  | f x = x : takeWhileRec f xs
  | otherwise = []

takeWhileFold :: (a -> Bool) -> [a] -> [a]
takeWhileFold f = foldr takeIt []
  where takeIt x acc | f x = x : acc
                     | otherwise = []

myGroupBy :: (a -> a -> Bool) -> [a] -> [[a]]
myGroupBy _ [] = []
myGroupBy f (x:xs) =
  foldl group [[x]] xs
  where group acc el 
          | f el (head $ last acc) = take (length acc - 1) acc ++ [last acc ++ [el]]
          | otherwise = acc ++ [[el]]
