module Page60 where

  -- Question 1
  data List a = Cons a (List a)
              | Nil
                deriving (Show)

  fromList :: List a -> [a]
  fromList Nil = []
  fromList (Cons v xs) = v : fromList xs

  data Maybe a = Just a
               | Nothing
               deriving (Show)

  -- Question 2
  data Tree a = Node a (Page60.Maybe (Tree a)) (Page60.Maybe (Tree a))
                deriving (Show)
