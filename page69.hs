module Page69 where

import Data.List

-- Exercise 1 / Exercise 2
myLength :: [a] -> Integer
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

-- Exercise 3
mean :: [Integer] -> Double
mean list = sum' / lengthInDouble
  where sum' = foldr (\a b -> fromIntegral a + b) 0 list
        lengthInDouble = fromIntegral (length list)

-- Exercise 4
palindrome :: [a] -> [a]
palindrome list = foldr (\a b -> b ++ [a]) list list

-- Exercise 5
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome [] = True
isPalindrome [_] = True
isPalindrome (x:xs)
  | x == last xs = isPalindrome (init xs)
  | otherwise = False

-- Exercise 6
mySort :: (Eq a) => [[a]] -> [[a]]
mySort list = sortBy (\a b -> if length a < length b then LT else GT) list

-- Exercise 7
myIntersperse :: [a] -> [[a]] -> [a]
myIntersperse _ [] = []
myIntersperse _ [x] = x
myIntersperse sep (x:xs) = x ++ sep ++ myIntersperse sep xs

-- Exercise 8
data Tree a = Node a (Tree a) (Tree a)
            | Empty
              deriving (Show)

binaryHeight :: Tree a -> Integer
binaryHeight Empty = 0
binaryHeight (Node _ left right) =
  let
      leftHeight = binaryHeight left + 1
      rightHeight = binaryHeight right + 1
  in
      if leftHeight < rightHeight
      then rightHeight
      else leftHeight

-- Exercise 9
data Direction = Left | Right | Straight
