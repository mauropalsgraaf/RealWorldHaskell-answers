module Page86 where

myHead :: [a] -> Maybe a
myHead [] = Nothing
myHead (x:_) = Just x

myTail :: [a] -> Maybe [a]
myTail [] = Nothing
myTail (_:xs) = Just xs

myInit :: [a] -> Maybe [a]
myInit [] = Nothing
myInit list = Just $ init list

myLast :: [a] -> Maybe a
myLast [] = Nothing
myLast [x] = Just x
myLast list = Just $ last list

splitWith :: [a] -> (a -> Bool) -> [[a]]
splitWith list func = splitWith' list func []

splitWith' :: [a] -> (a -> Bool) -> [[a]] -> [[a]]
splitWith' [] _ result = result
splitWith' (x:xs) func result
  | null result = splitWith' xs func [[x]]
  | func x = let
      result' = take (length result - 1) result ++ [last result ++ [x]]
    in
      splitWith' xs func result'
  | otherwise = splitWith' xs func $ result ++ [[x]]

myTranspose :: String -> String
myTranspose input =
  let
    lines' = lines input
  in
    input
